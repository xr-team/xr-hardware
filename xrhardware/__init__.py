#!/usr/bin/env python3 -i
# Copyright 2020 Collabora, Ltd
# SPDX-License-Identifier: BSL-1.0
#
# Original Author: Rylie Pavlik <rylie.pavlik@collabora.com>
"""XR hardware database and associated utilities."""
